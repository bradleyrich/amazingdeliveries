﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AmazingDeliveries.Models;

namespace AmazingDeliveries.Controllers
{
    public class RecipientsController : ApiController
    {
        private AmazingDeliveriesContext db = new AmazingDeliveriesContext();

        // GET: api/Recipients
        public IQueryable<Recipient> GetRecipients()
        {
            return db.Recipients;
        }

        // GET: api/Recipients/5
        [ResponseType(typeof(Recipient))]
        public async Task<IHttpActionResult> GetRecipient(int id)
        {
            Recipient recipient = await db.Recipients.FindAsync(id);
            if (recipient == null)
            {
                return NotFound();
            }

            return Ok(recipient);
        }

        // PUT: api/Recipients/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRecipient(int id, Recipient recipient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != recipient.RecipientId)
            {
                return BadRequest();
            }

            db.Entry(recipient).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipientExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Recipients
        [ResponseType(typeof(Recipient))]
        public async Task<IHttpActionResult> PostRecipient(Recipient recipient)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Recipients.Add(recipient);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = recipient.RecipientId }, recipient);
        }

        // DELETE: api/Recipients/5
        [ResponseType(typeof(Recipient))]
        public async Task<IHttpActionResult> DeleteRecipient(int id)
        {
            Recipient recipient = await db.Recipients.FindAsync(id);
            if (recipient == null)
            {
                return NotFound();
            }

            db.Recipients.Remove(recipient);
            await db.SaveChangesAsync();

            return Ok(recipient);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RecipientExists(int id)
        {
            return db.Recipients.Count(e => e.RecipientId == id) > 0;
        }
    }
}