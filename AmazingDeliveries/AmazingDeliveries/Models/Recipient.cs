﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AmazingDeliveries.Models
{
    public class Recipient
    {
        public int RecipientId { get; set; }

        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "Recipient First Name")]    
        public string FirstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "Recipient Surname")]
        public string Surname { get; set; }

        [Required]
        [StringLength(300, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 5)]
        [Display(Name = "Recipient Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "Recipient Postcode")]
        public string Postcode { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Please enter a valid phone number", MinimumLength = 2)]
        [Display(Name = "Recipient Contact Number")]
        public string ContactNumber { get; set; }

        [Required]
        [Display(Name = "Parcel Size")]
        public string ParcelSize { get; set; }

        [Required]
        [Display(Name = "Delivery Time")]
        public string DeliverTime { get; set; }

        public Customer Customer { get; set; }

    }
}