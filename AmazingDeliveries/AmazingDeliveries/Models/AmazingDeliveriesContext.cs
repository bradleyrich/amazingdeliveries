﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace AmazingDeliveries.Models
{
    public class AmazingDeliveriesContext : DbContext
    {
        //Added SaveChanges override to get a verbose exception as all it displaying by default is your entities are not valid and thats just not helpful
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public AmazingDeliveriesContext() : base("name=AmazingDeliveriesContext")
        {
        }

        public System.Data.Entity.DbSet<AmazingDeliveries.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<AmazingDeliveries.Models.Recipient> Recipients { get; set; }
    
    }
}
