﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazingDeliveries.Models
{
    public class CustomerDetailsDto
    {
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Address { get; set; }

        public string Postcode { get; set; }

        public string ContactNumber { get; set; }

        public string EmailAddress { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public List<Recipient> Recipients { get; set; }
    }
}