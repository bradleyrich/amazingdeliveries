﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AmazingDeliveries.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
            
        [Required]
        [StringLength(100, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        //To do would be to go off to a third party tool and verify the address
        [Required]
        [StringLength(300, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 5)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(10, ErrorMessage = "Please enter your name it must be at least {2} characters long", MinimumLength = 2)]
        [Display(Name = "Postcode")]
        public string Postcode { get; set; }

        [Required]
        [StringLength(20, ErrorMessage = "Please enter a valid phone number", MinimumLength = 6)]
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string EmailAddress { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [NotMapped]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public ICollection<Recipient> Recipients { get; set; }
    }
}
