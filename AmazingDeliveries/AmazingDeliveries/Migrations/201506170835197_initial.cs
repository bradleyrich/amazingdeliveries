namespace AmazingDeliveries.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        Surname = c.String(nullable: false, maxLength: 100),
                        Address = c.String(nullable: false, maxLength: 300),
                        Postcode = c.String(nullable: false, maxLength: 10),
                        ContactNumber = c.String(nullable: false, maxLength: 20),
                        EmailAddress = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Recipients",
                c => new
                    {
                        RecipientId = c.Int(nullable: false, identity: true),
                        CustomerId = c.Int(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        Surname = c.String(nullable: false, maxLength: 100),
                        Address = c.String(nullable: false, maxLength: 300),
                        Postcode = c.String(nullable: false, maxLength: 10),
                        ContactNumber = c.String(nullable: false, maxLength: 20),
                        ParcelSize = c.String(nullable: false),
                        DeliverTime = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RecipientId)
                .ForeignKey("dbo.Customers", t => t.CustomerId, cascadeDelete: true)
                .Index(t => t.CustomerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Recipients", "CustomerId", "dbo.Customers");
            DropIndex("dbo.Recipients", new[] { "CustomerId" });
            DropTable("dbo.Recipients");
            DropTable("dbo.Customers");
        }
    }
}
