namespace AmazingDeliveries.Migrations
{
    using AmazingDeliveries.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AmazingDeliveries.Models.AmazingDeliveriesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AmazingDeliveries.Models.AmazingDeliveriesContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Customers.AddOrUpdate(x => x.CustomerId,
              new Customer() { FirstName = "f111", Surname = "s111", Address = "add11111", Postcode = "p11", ContactNumber = "02476111111", EmailAddress = "e1@e1.com", Password = "pass1", ConfirmPassword = "pass1" },
              new Customer() { FirstName = "f222", Surname = "s222", Address = "add22222", Postcode = "p22", ContactNumber = "02476222222", EmailAddress = "e2@e2.com", Password = "pass2", ConfirmPassword = "pass2" },
              new Customer() { FirstName = "f333", Surname = "s333", Address = "add33333", Postcode = "p33", ContactNumber = "02476333333", EmailAddress = "e3@e3.com", Password = "pass3", ConfirmPassword = "pass3" },
              new Customer() { FirstName = "f444", Surname = "s444", Address = "add44444", Postcode = "p44", ContactNumber = "02476444444", EmailAddress = "e4@e4.com", Password = "pass4", ConfirmPassword = "pass4" }
              );

            context.Recipients.AddOrUpdate(x => x.RecipientId,
                new Recipient() { CustomerId = 4, FirstName = "f4", Surname = "s4", Address = "add44444", Postcode = "p4", ContactNumber = "02476111111", ParcelSize = "large", DeliverTime = "morning" },
                new Recipient() { CustomerId = 3, FirstName = "f3", Surname = "s3", Address = "add3333", Postcode = "p3", ContactNumber = "02476111111", ParcelSize = "small", DeliverTime = "afternoon" },
                new Recipient() { CustomerId = 2, FirstName = "f2", Surname = "s2", Address = "add2222", Postcode = "p2", ContactNumber = "02476111111", ParcelSize = "medium", DeliverTime = "lunch" },
                new Recipient() { CustomerId = 1, FirstName = "f1", Surname = "s1", Address = "add1111", Postcode = "p1", ContactNumber = "02476111111", ParcelSize = "small", DeliverTime = "morning" }
                );
        }
    }
}
